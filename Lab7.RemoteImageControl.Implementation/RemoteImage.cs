﻿using Lab7.RemoteImageControl.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace Lab7.RemoteImageControl.Implementation
{
    public class RemoteImage : IRemoteImage
    {
        Control control = null;
        public Control Control
        {
            get { return control; }
        }

        public void Load(string url)
        {
            MemoryStream ms = new MemoryStream(new WebClient().DownloadData(url));
            ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
            ImageSource imageSource = (ImageSource)imageSourceConverter.ConvertFrom(ms);
            (control as RemoteImageCtrl).ImageC.Source = imageSource;
        }
    }
}
