﻿using Lab7.AddressControl.Contract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
namespace Lab7.AddressControl
{
    public class Address : IAddress
    {
        private string url = null;
        public string Url
        {
            get
            {
                return url;
            }
            set
            {
                url = value;
            }
        }
        Control kontrolka = new AdressView();
        

        public Control Control
        {
            get
            {
                return kontrolka;
            }
        }
            
        public event EventHandler<AddressChangedArgs> AddressChanged;


    }
}
